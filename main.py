import numpy as np
import time 
import itertools
import matplotlib.pyplot as plt

SUN_COST = 10
WIND_COST = 10
HOUSE_COST = 10
HOSPITAL_COST = 10
FACTORY_COST = 10

FACTORY_RATIO = 3
HOSPITAL_RATIO = 5

MAX_RISK = 0    # Суммарное количество всех компонентов системы (дом, заводы, больницы)

MAX_RISK += 3 *FACTORY_RATIO   # factory
MAX_RISK += 3 *HOSPITAL_RATIO   # hospital
MAX_RISK += 4   # house

t = time.time()

def auk():
    try:
        sun, wind, hosp, fact, house = np.loadtxt('./forecast.csv', unpack=True, skiprows=1)

        iter = itertools.combinations_with_replacement('012345678', 5)
        sums = []
        ens = []
        for c in iter:
            s, w, h, f, h_ = map(int, c)
            if  s > 3 or s < 1 or w > 2 or h > 3 or f > 3 or h_ < 1 or h_ > 4:
                continue

            sum = 0
            energy = 0
            risk = 0
            for i in range(len(sun)):

                wind_energy = w * wind[i] * 1.59
                if wind_energy > 15:
                    wind_energy = 15

                sun_energy = s * sun[i] * 1.05
                if sun_energy > 15:
                    sun_energy = 15
                elif sun_energy < 4:
                    sun_energy = 4

                energy += (sun_energy
                    + wind_energy
                    - h * hosp[i]
                    - f * fact[i]
                    - h_ * house[i])

                sum += (int(HOSPITAL_COST) * h * hosp[i]
                    + int(FACTORY_COST) * f * fact[i]
                    + int(HOUSE_COST) * h_ * house[i]
                    - int(SUN_COST) * s * sun[i]
                    - int(WIND_COST) * w * wind[i])

            if energy < 0:
                continue

            risk = h *HOSPITAL_RATIO + f *FACTORY_RATIO + h_

            risk = risk /MAX_RISK *100  # риск в процентах

            # round
            sum = round(sum, ndigits=2)
            risk = round(risk, ndigits=2)

            sums.append((sum, c, energy, risk))

        # sums = sorted(sums, key=lambda x: abs(x[2]))
        sums = sorted(sums, key=lambda x: -x[0])
    # print(sums[0])
        print(f"Количество батарей: {sums[0][1][0]}, Количество ветряков: {sums[0][1][1]}, Количество домов:{sums[0][1][2]}, Количество заводов: {sums[0][1][3]}, Количество больниц:{sums[0][1][4]}")
        print(f"Предполагаемая прибыль: {sums[0][0]}")
        print('----', time.time() - t, 'seconds ----')
        return sums
    except:
        print("File not open!")
        return [(0, (0, 0, 0, 0, 0), 0, 0)]