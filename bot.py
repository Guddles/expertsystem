import telebot
from telebot import types
import main
from main import auk

MAX_KIT_COUNT = 3

API_TOKEN = "6866397922:AAHWxmDJes62ARqRznKonICTgWtPdPxDnNU"

bot = telebot.TeleBot(API_TOKEN)
bot.set_webhook()
flagFileWait = False

def input_sun_price(message):
    main.SUN_COST = int(message.text) # Это и будет текст, которой отправил пользователь
    main.auk()
    print("Солнце", message.text)

def input_wind_price(message):
    main.WIND_COST = message.text # Это и будет текст, которой отправил пользователь
    main.auk()
    print("ветер", message.text)

def input_house_price(message):
    main.HOUSE_COST = message.text # Это и будет текст, которой отправил пользователь
    main.auk()
    print("дом", message.text)

def input_hospital_price(message):
    main.HOSPITAL_COST = message.text # Это и будет текст, которой отправил пользователь
    main.auk()
    print("больница", message.text)

def input_factory_price(message):
    main.FACTORY_COST = message.text # Это и будет текст, которой отправил пользователь
    main.auk()
    print("завод", message.text)

def backToMenu(message):
    bot.send_message(message.chat.id,'TESAT')


@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id,'Привет')
    markup=types.ReplyKeyboardMarkup(resize_keyboard=True)
    addFileButton = types.KeyboardButton("Отправить файл прогноз")
    summariseButton = types.KeyboardButton("Рассчитать")
    changePriceButton = types.KeyboardButton("Изменить цены")
    buttons_list = ["Отправить файл прогноза", "Рассчитать", "Изменить цены"]
    for i in buttons_list:
        markup.add(i)
    bot.send_message(message.chat.id,'Выберите что вам надо', reply_markup=markup)


@bot.message_handler(content_types=['document'])
def buttons_replay(message):
    global flagFileWait
    try:
        if flagFileWait:
            file_info = bot.get_file(message.document.file_id)
            
            src = "C:/prosekino/expertsystem/forecast.csv"# + message.document.file_name
            download_file = bot.download_file(file_info.file_path)
            with open(src, "wb") as f:
                f.write(download_file)
            bot.send_message(message.chat.id, "Я сохранил")
            flagFileWait = False
        else:
            bot.send_message(message.chat.id, "Файл не ожидался")
    except Exception as e:
        bot.send_message(message.chat.id, e)

@bot.message_handler(commands=['changeCost'])
def changeCost(message):
    keyboard = types.InlineKeyboardMarkup()
    buttonChangeHouseCost = types.InlineKeyboardButton(text="Цена дома", callback_data="changeCostHouse")
    buttonChangeHospitalCost = types.InlineKeyboardButton(text="Цена больницы", callback_data="changeCostHospital")
    buttonChangeFactoryCost = types.InlineKeyboardButton(text="Цена завода", callback_data="changeCostFactory")
    buttonChangeSunCost = types.InlineKeyboardButton(text="Цена солнечной панели", callback_data="changeCostSun")
    buttonChangeWindCost = types.InlineKeyboardButton(text="Цена ветряков", callback_data="changeCostWind")
    keyboard.add(buttonChangeHouseCost, buttonChangeFactoryCost, buttonChangeSunCost, buttonChangeWindCost, buttonChangeHospitalCost)
    bot.send_message(message.chat.id, "Выберите объект для смены цены", reply_markup=keyboard)
    # return keyboard


@bot.callback_query_handler(func=lambda call: True)
def change(call):
    # print("кол бэк: ",call.data)
    message = call.message
    chat_id = message.chat.id
    message_id = message.id
    # message = bot.send_message(message, "Введите цену")
    if call.data == "changeCostSun":
        markup = types.InlineKeyboardMarkup()
        back_button = types.InlineKeyboardButton(text="Назад", callback_data="backToMenu")
        markup.add(back_button)
        bot.register_next_step_handler(message, input_sun_price)
        bot.send_message(call.message.chat.id, """Вы выбрали Солнечную электростанцию. Отправьте новую цену или нажмите 'Назад', чтобы вернуться в главное меню.""", 
                                                 reply_markup=markup)
    elif call.data == "changeCostHouse":
        markup = types.InlineKeyboardMarkup()
        back_button = types.InlineKeyboardButton(text="Назад", callback_data="backToMenu")
        
        markup.add(back_button)
        bot.send_message(call.message.chat.id, """Вы выбрали Дом. Отправьте новую цену или нажмите 'Назад', чтобы вернуться в главное меню.""", 
                                                 reply_markup=markup)
        bot.register_next_step_handler(message, input_house_price)
    elif call.data == "changeCostHospital":
        markup = types.InlineKeyboardMarkup()
        back_button = types.InlineKeyboardButton(text="Назад", callback_data="backToMenu")
        
        markup.add(back_button)
        bot.send_message(call.message.chat.id, """Вы выбрали Больницу. Отправьте новую цену или нажмите 'Назад', чтобы вернуться в главное меню.""", 
                                                 reply_markup=markup)
        bot.register_next_step_handler(message, input_hospital_price)
    elif call.data == "changeCostWind":
        markup = types.InlineKeyboardMarkup()
        back_button = types.InlineKeyboardButton(text="Назад", callback_data="backToMenu")
        
        markup.add(back_button)
        bot.send_message(call.message.chat.id, """Вы выбрали Ветрянную станцию. Отправьте новую цену или нажмите 'Назад', чтобы вернуться в главное меню.""", 
                                                 reply_markup=markup)
        bot.register_next_step_handler(message, input_wind_price)
    elif call.data == "changeCostFactory":
        markup = types.InlineKeyboardMarkup()
        back_button = types.InlineKeyboardButton(text="Назад", callback_data="backToMenu")
        
        markup.add(back_button)
        bot.send_message(call.message.chat.id, """Вы выбрали Завод. Отправьте новую цену или нажмите 'Назад', чтобы вернуться в главное меню.""", 
                                                 reply_markup=markup)
        bot.register_next_step_handler(message, input_factory_price)
    elif call.data == "backToMenu":
        bot.answer_callback_query(call.id, changeCost(message))
        # bot.send_message(message_id, reply_markup=changeCost())
        print(message.text)
        bot.clear_step_handler_by_chat_id(chat_id=call.message.chat.id)
        # bot.send_message(message.chat.id, reply_markup=changeCost(message))
        # print("msg: ",message)
        # changeCost()

@bot.message_handler(content_types='text')
def message_reply(message):
    global flagFileWait
    if message.text == "Отправить файл прогноза":
        bot.send_message(message.chat.id, "Отправьте файл")
        flagFileWait = True
    elif message.text == "Рассчитать":

        sums = auk()
        
        kitCounter = 0
        for kit in sums:
            msg = ""
            msg += f"\t\t--- Набор {kitCounter +1} ---\n"
            msg += f"\tКонфигурация\n"
            msg += f"Количество батарей: {sums[kitCounter][1][0]}\nКоличество ветряков: {sums[kitCounter][1][1]}\nКоличество домов: {sums[kitCounter][1][2]}\nКоличество заводов: {sums[kitCounter][1][3]}\nКоличество больниц: {sums[kitCounter][1][4]}\n"
            msg += f"\tРассчеты\n"
            msg += f"Предполагаемая прибыль: {sums[kitCounter][0]}\nРиск: {sums[kitCounter][3]}%"
            bot.send_message(message.chat.id, msg)           

            kitCounter += 1
            if kitCounter >= MAX_KIT_COUNT:
                break
    elif message.text == "Изменить цены":
        msg = f"Текущие цены:\n Солнечная станция: {main.SUN_COST}\n Ветрянная станция: {main.WIND_COST}\n Дома: {main.HOUSE_COST}\n Болницы: {main.HOSPITAL_COST}\n Заводы станция: {main.FACTORY_COST} "
        bot.send_message(message.chat.id, msg)
        changeCost(message)
        # bot.register_next_step_handler(message, changeCost)
    else:
        bot.send_message(message.chat.id,"неизвестная команда")

bot.infinity_polling()

